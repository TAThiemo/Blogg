---
title: Spelletjes spellen
subtitle: Door Tim Oskam
date: 2017-09-20
---

# **20.09.17**

Vandaag hebben wij 3 spellen gespeeld voor de spel analyse het was niet mijn taak om te waarnemen, ik heb dus de spellen gespeeld. We hebben : 30 seconds, skipbo en weerwolven van wakkerdam gedaan. super gezellig. Daarna was het tijd voor mijn workshop: Analyseren als onderzoeksmethode. Hier hebben we geleerd dat hoe je het beste kan waarnemen en wat waarnemen zowel observeren als interpreteren is. En hoe je een plan kan maken en op welke manieren je dit kan toepassen.

>Vandaag heb ik geleerd:
>- Wat Analyseren, Waarnemen en Interpreteren is.
>- Hoe zet je een analyse-plan op.
>- Op welke manier kun je analyseren en hoe kun je het beste analyseren.
