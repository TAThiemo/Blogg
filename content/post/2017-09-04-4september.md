---
title: Het begin
subtitle: Door Tim Oskam
date: 2017-09-04
---

# **04.09.17**
Vandaag heb ik samen met mijn team een logo gemaakt. We heten 'Creative Collectors' en hebben een strak logo ontworpen.

Hierna zijn we begonnen met het kijken wie wat kan binnen ons team, wie de teamcaptain werd we hebben besloten dat ik de teamcaptain ben geworden en hebben we onze teamafspraken besproken.

Hierna zijn we begonnen met het maken van mindmaps. We hebben een mindmap gemaakt voor: 

>- Games
>- Doelgroep
>- Wat is een game
>- Rotterdam
>- de visie van de HR 

We hebben hierna nog wat gebrainstormd over de eerste ideeën van onze game. Maar eerst moeten we nog verder onderzoek doen.