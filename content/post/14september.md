---
title: Werkcollege
subtitle: Door Tim Oskam
date: 2017-09-14
---

# **14.09.17**
Vandaag hebben we werkcollege gehad over de theory die we dinsdag gekregen hebben, ook heb ik mijn 1e keuzevak gehad (photoshop). Bij het werkcollege hebben we het vooral over het visualiseren van beeld gehad. We kregen woorden en hierbij moesten wij binnen 10 seconden een tekening bij maken. Later gingen we kijken wat ieder had getekend.

Later moesten we dit nogmaals doen maar dan moest alles abstract getekend worden dit vond ik moeilijker, maar niet onmogelijk. Ik tekende gewoon het eerste dat in mij opkwam als vorm. Bij het nakijken zat ik met sommige beelden goed en sommige niet, daarna moesten we de 5 foto's die je moest meenemen naar het werkcollege analyseren en indelen in:

- Gestalt wetten
- Semiotiek
- Rotorica

Dit vond ik nog wel lastig, daarom wil ik dat boek nog gaan doorlezen. Helaas is die momenteel nog offline in de mediatheek. 

Bij photoshop hebben we de 1e paar tools gehad zelf had ik al wat verstand van photoshop. Maar ik heb toch wat geleerd hoe ik een zon kan maken in een afbeelding een afbeelding wat beweging kan geven en hoe ik goed om kan gaan met de pen-tool. Ook hebben we een huiswerk opdracht gekregen voor volgende week.